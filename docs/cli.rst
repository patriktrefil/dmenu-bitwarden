Command Line Options
====================

If there are multiple matches for selected entry (defined by its "name" and "username"),
the first one is copied to the clipboard.

.. argparse::
   :module: cli
   :func: create_parser
   :prog: dmenu_bitwarden.py
